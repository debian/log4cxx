Source: log4cxx
Section: devel
Priority: optional
Maintainer: Tobias Frost <tobi@debian.org>
Homepage: https://logging.apache.org/log4cxx/index.html
Vcs-Git: https://salsa.debian.org/debian/log4cxx.git
Vcs-Browser: https://salsa.debian.org/debian/log4cxx
Build-Depends: cmake,
               debhelper-compat (= 13),
               libapr1-dev,
               libaprutil1-dev,
               zip
Build-Depends-Indep: doxygen <!nodoc>,
                     graphviz <!nodoc>
Standards-Version: 4.7.0
Rules-Requires-Root: no

Package: liblog4cxx-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libapr1-dev,
         libaprutil1-dev,
         liblog4cxx15 (= ${binary:Version}),
         ${misc:Depends}
Suggests: liblog4cxx-doc (= ${source:Version}), pkgconf
Description: Logging library for C++ (development files)
 Log4cxx is the C++ port of log4j, a logging framework for JAVA.
 Log4cxx attempts to mimic log4j usage as much as the language will
 allow and to be compatible with log4j configuration and output formats.
 .
 This package provides the development files.

Package: liblog4cxx15
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: zip
Description: Logging library for C++
 Log4cxx is the C++ port of log4j, a logging framework for JAVA.
 Log4cxx attempts to mimic log4j usage as much as the language will
 allow and to be compatible with log4j configuration and output formats.

Package: liblog4cxx-doc
Build-Profiles: <!nodoc>
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Documentation for log4cxx
 Log4cxx is the C++ port of log4j, a logging framework for JAVA.
 Log4cxx attempts to mimic log4j usage as much as the language will
 allow and to be compatible with log4j configuration and output formats.
 .
 This package provides doxygen documentation for the library.
